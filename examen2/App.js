import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { DataTable, Button } from 'react-native-paper';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [filtrarXtodos, setFiltrarXTodos] = useState([]);
  const [opcionElegida, setopcionElegida] = useState('');

  useEffect(() => {
    fetchTodos();
  }, []);

  const fetchTodos = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
    } catch (error) {
      console.error('Error al buscar en Json:', error);
    }
  };

  const handleFiltro = (filterType) => {
    setopcionElegida(filterType);
    switch (filterType) {
      case 'soloIDs':
        setFiltrarXTodos(todos.map(todo => ({ id: todo.id })));
        break;
      case 'IDsYTitulos':
        setFiltrarXTodos(todos.map(todo => ({ id: todo.id, title: todo.title })));
        break;
      case 'IDsYTitulosSinResolver':
        setFiltrarXTodos(todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title })));
        break;
      case 'IDsYTitulosResueltos':
        setFiltrarXTodos(todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title })));
        break;
      case 'soloIDsYUserIDs':
        setFiltrarXTodos(todos.map(todo => ({ id: todo.id, userId: todo.userId })));
        break;
      case 'IDsYUserIDsResueltos':
        setFiltrarXTodos(todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId })));
        break;
      case 'IDsYUserIDsSinResolver':
        setFiltrarXTodos(todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId })));
        break;
      default:
        setFiltrarXTodos([]);
        break;
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.scrollViewContent}>
      <View style={styles.container}>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('soloIDs')} style={styles.button}>
              Lista de todos los pendientes (solo IDs)
            </Button>
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('IDsYTitulos')} style={styles.button}>
              Lista de todos los pendientes (IDs y Titles)
            </Button>
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('IDsYTitulosSinResolver')} style={styles.button}>
              Lista de todos los pendientes sin resolver (ID y Title)
            </Button>
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('IDsYTitulosResueltos')} style={styles.button}>
              Lista de todos los pendientes resueltos (ID y Title)
            </Button>
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('soloIDsYUserIDs')} style={styles.button}>
              Lista de todos los pendientes (IDs y userID)
            </Button>
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('IDsYUserIDsResueltos')} style={styles.button}>
              Lista de todos los pendientes resueltos (ID y userID)
            </Button>
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={() => handleFiltro('IDsYUserIDsSinResolver')} style={styles.button}>
              Lista de todos los pendientes sin resolver (ID y userID)
            </Button>
          </View>
        </View>

        {opcionElegida !== '' && filtrarXtodos.length > 0 && (
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>ID</DataTable.Title>
              <DataTable.Title>User ID</DataTable.Title>
              <DataTable.Title>Title</DataTable.Title>
              <DataTable.Title>Completed</DataTable.Title>
            </DataTable.Header>

            {filtrarXtodos.map(todo => (
              <DataTable.Row key={todo.id}>
                <DataTable.Cell>{todo.id}</DataTable.Cell>
                <DataTable.Cell>{todo.userId}</DataTable.Cell>
                <DataTable.Cell>{todo.title}</DataTable.Cell>
                <DataTable.Cell>{todo.completed ? 'Yes' : 'No'}</DataTable.Cell>
              </DataTable.Row>
            ))}
          </DataTable>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
  },
  buttonsContainer: {
    marginBottom: 16,
  },
  buttonContainer: {
    marginBottom: 8,
  },
  button: {
    width: '100%',
  },
});

export default App;
